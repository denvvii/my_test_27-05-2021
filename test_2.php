<?php

$arr = [4, 5, 8, 9, 1, 7, 2];

function array_swap(&$arr, $num)
{
    $val_zero = $arr[0];
    $val_num = $arr[$num];
    $arr[0] = $val_num;
    $arr[$num] = $val_zero;
}

$max_key = $max_val = 0;
foreach ($arr as $key => $value) {
    if ($key > $max_key)
        $max_key = $key;
    if ($value > $max_val)
        $max_val = $value;
}

$factorial = function ($arr) use (&$factorial, &$max_key, &$max_val) {
    if ($max_key <= 0)
        return $arr;
    foreach ($arr as $key => $value) {
        if ($value == $max_val && $key <= $max_key) {
            if ($key > 0)
                array_swap($arr, $key);
            array_swap($arr, $max_key);
            $max_key--;
            $max_val = 0;
            foreach ($arr as $key_2 => $value_2) {
                if ($key_2 <= $max_key) {
                    if ($value_2 > $max_val)
                        $max_val = $value_2;
                }
            }
        }
    }
    return $factorial($arr);
};

print_r($factorial($arr));