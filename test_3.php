<?php

class PageParser{

    private $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public function getContentForUrl()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: text/html']);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $html = curl_exec($ch);
        if(curl_error($ch))
            throw new Exception(print_r(curl_error($ch)));
        curl_close($ch);
        return $html;
    }

    public function getArrayTagsForHTML(string $html): array
    {
        preg_match_all("/<([^>\s\/]+)[^>]*>/i", $html, $matches, PREG_PATTERN_ORDER);
        return $matches[1] ?? [];
    }
}

class View{

    public static function printArray(array $arr)
    {
        foreach ($arr as $key => $value){
            print "{$key}: {$value}" . PHP_EOL;
        }
    }
}

$url = "https://www.php.net/";
try {
    $parser = new PageParser($url);
    $html = $parser->getContentForUrl();
    $array_tags = $parser->getArrayTagsForHTML($html);
    $count_tags = array_count_values($array_tags);
    View::printArray($count_tags);
} catch(Exception $ex) {
    echo $ex->getMessage();
}