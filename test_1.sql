create table contacts
(
    id int auto_increment primary key,
    name varchar(100) not null
);

create table friends
(
    contact_id int null,
    friend_id  int null,
    constraint friends_contacts_id_contact foreign key (contact_id) references contacts (id),
    constraint friends_contacts_id_friend foreign key (friend_id) references contacts (id)
);

select c.name from contacts as c join friends f on c.id = f.contact_id group by c.name having count(*) > 5;

select * from contacts as c
    join friends f on c.id = f.contact_id
    join contacts c2 on c2.id = f.friend_id
where
      f.id in (select f1.id from friends f1, friends f2 where f1.contact_id = f2.friend_id and f1.friend_id = f2.contact_id);